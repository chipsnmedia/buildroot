# Quick build instructions

### checkout buildroot
assuming `~/development/chips-media` as a toplevel container directory
```
mkdir ~/development/chips-media/buildroot
cd ~/development/chips-media/buildroot
git clone git@gitlab.collabora.com:chipsnmedia/buildroot.git .
```

### checkout a kernel branch
If you already have the kernel source, you can use your own directory and skip this step.
Just replace the paths with the correct location.
```
mkdir ~/development/chips-media/linux
cd ~/development/chips-media/linux
git clone git@gitlab.collabora.com:chipsnmedia/kernel.git .
git checkout <branch>
```

### build the image
```
cd ~/development/chips-media/buildroot
make beaglev_defconfig
make LINUX_OVERRIDE_SRCDIR=~/development/chips-media/linux all
```

this will build the entire OS, and place the output in `output/images`

### create a bootable SD card
assuming your mmc card drive is `/dev/mmcblk0`
```
dd if=~/development/chips-media/buildroot/output/images/sdcard.img of=/dev/mmcblk0 bs=16M conv=fsync
```

### buildroot misc stuff
##### configure buildroot (add packages, set settings etc)
OS configuration is done via a KConfig interface. Anyone familiar with kernel work should feel comfortable with this
```
make menuconfig
```
once you have done any changes you require, build as above.

##### overriding source directories
By default buildroot will use the source packages indicated in the package recipes found in `packages/`.
To use your own source, as we do above for the kernel, you can update the recipe file for a new release,
or to develop via buildroot, where you often change branches or want to use code that is not committed yet,
ou can override the source location via

```
make <PACKAGE>_OVERRIDE_SRCDIR=<path to source>
```

The package needs to be the package name as found in the `packages/` directory.
For the kernel, see above.
Whenever you change any code in that directory and want to rebuild it, you need to tell buildroot that it has changed.
(It doesn't have dependencies on the source files of custom directories). So, to force it to rebuild the kernel from your
own source directory, the rebuild command is:

```
make LINUX_OVERRIDE_SRCDIR=/home/bob/development/linux linux-rebuild all
```
i.e. `<package-name>-rebuild` forces it to rebuild that package.
It will rsync the source (only the changed files) to it's build area and do the build

##### adding misc files
If you want to add some other files to the filesystem, but don't want to write a recipe, you can
add the files to `board/beaglev/rootfs_overlay/`. Anything in there will be added relative to
the root directory.
This can be useful for adding test files etc that you wouldn't want to publish a recipe for.


### buildroot documentation
The main buildroot manual can be found here:
https://buildroot.org/downloads/manual/manual.html

